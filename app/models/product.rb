class Product < ApplicationRecord
  belongs_to :user
  belongs_to :product_category, optional: true

  validates_presence_of :name, :user_id
end
