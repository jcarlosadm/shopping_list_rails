class ProductCategory < ApplicationRecord
	belongs_to :user
	has_many :products, dependent: :nullify

	validates_presence_of :name, :user_id
end
