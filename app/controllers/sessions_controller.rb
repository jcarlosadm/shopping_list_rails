class SessionsController < ApplicationController
	skip_before_filter :require_login

  	def new
  	end

  	def create
		user = User.find_by(email: params[:session][:email].downcase)
		if user && user.authenticate(params[:session][:password])
			log_in user
			redirect_to user
		else
			flash.now[:danger] = t('.error')
			render 'new'
		end
  	end

	def destroy
		log_out
		redirect_to root_url
	end
end
