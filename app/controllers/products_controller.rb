class ProductsController < ApplicationController

	def index
		@products = get_sorted_products
		@new_product = get_products.new
	end

	def create
		@new_product = get_products.new(product_params)

		if @new_product.save
			flash[:notice] = t('.success')
			redirect_to action: 'index'
		else
			flash[:notice] = t('.error')
			@products = get_sorted_products
			render action: 'index'
		end
	end

	def show
		flash[:notice] = nil
		redirect_to action: 'index'
	end

	def update
		@edit_product = get_products.find(params[:id])

		if @edit_product.update_attributes(product_params)
			flash[:notice] = t('.success')
			redirect_to action: 'index'
		else
			@products = get_sorted_products
			@new_product = get_products.new
			flash[:notice] = t('.error')
			render action: 'index'
		end
	end

	def destroy
		@product = get_products.find(params[:id])
		@product.destroy

		flash[:notice] = t('.success')
		redirect_to action: 'index'
	end

	private

	def product_params
		params.require(:product).permit!
	end

	def get_products
		return current_user.products
	end

	def get_sorted_products
		return get_products.all.sort_by {|f| f.name}
	end

end
