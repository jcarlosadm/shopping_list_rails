class ProductCategoriesController < ApplicationController

    def index
        @product_categories = get_sorted_categories
        @product_category_new = current_user.product_categories.new
    end

	def create
		@product_category_new = current_user.product_categories.new(category_params)
        if @product_category_new.save
			flash[:notice] = t('.success')
            redirect_to action: "index"
        else
			flash[:notice] = t('.error')
			@product_categories = get_sorted_categories
            render action: "index"
        end
	end

	def show
		flash[:notice] = ""
		redirect_to action: "index"
	end

    def update
		@product_category_edit = current_user.product_categories.find(params[:id])

		if @product_category_edit.update_attributes(category_params)
			flash[:notice] = t('.success')
			redirect_to action: "index"
		else
			@product_categories = get_sorted_categories
			@product_category_new = current_user.product_categories.new
			flash[:notice] = t('.error')
			render action: "index"
		end
    end

	def destroy
		@product_category = current_user.product_categories.find(params[:id])
		@product_category.destroy

		flash[:notice] = t('.success')
		redirect_to action: "index"
	end

	private

	def category_params
		params.require(:product_category).permit!
	end

	def get_sorted_categories
		return current_user.product_categories.all.sort_by {|f| f.name.downcase}
	end
end
