class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  before_filter :set_locale, :require_login

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options
      { :locale => I18n.locale }
  end

  private

  def require_login
	  unless logged_in?
		  redirect_to login_url
	  end
  end
end
