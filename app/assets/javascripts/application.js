// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require_tree .

newBoxClose = function() {
	document.getElementById('new_box').style.display = "none";
}

newBoxOpen = function() {
	document.getElementById('new_box').style.display = "block";
}

editBoxOpen = function(id) {
	document.getElementById('edit_box_'+id).style.display = "block";
}

editBoxClose = function(id) {
	document.getElementById('edit_box_'+id).style.display = "none";
}
