class AddUserToCategories < ActiveRecord::Migration[5.0]
  def change
    add_reference :product_categories, :user, foreign_key: true
  end
end
