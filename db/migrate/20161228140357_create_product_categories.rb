class CreateProductCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :product_categories do |t|
      t.string :name
      t.text :description
      t.string :url_image

      t.timestamps
    end
  end
end
