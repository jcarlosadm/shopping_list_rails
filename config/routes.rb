Rails.application.routes.draw do
    LOCALES = /en|pt\-BR/

    scope ":locale", :locale => LOCALES do
		get '/signup', to: 'users#new'
		get '/login', to: 'sessions#new'
		post '/login', to: 'sessions#create'
		delete '/logout', to: 'sessions#destroy'

	    resources :product_categories
		resources :users
		resources :sessions
		resources :products
	end

    match '/:locale' => 'home#index', :via => [:get], ':locale' => LOCALES
	root :to => "home#index"

	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
